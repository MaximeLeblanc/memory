import React, { Component } from 'react'
import shuffle from 'lodash.shuffle'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Navbar from 'react-bootstrap/Navbar'

import './App.css'
import card1 from './card1.png' 

import Card from './Card'
import GuessCount from './GuessCount'
import HallOfFame, { FAKE_HOF } from './HallOfFame'

const VISUAL_PAUSE_MSECS = 750
const X_LENGTH = 5
const Y_LENGTH = 4
const SYMBOLS = '0123456789'

class App extends Component {

  state = {
    cards: this.generateCards(),
    currentPair: [],
    guesses: 0,
    matchedCardIndex: [],
  }

  generateCards() {
    const result = []
    const size = X_LENGTH * Y_LENGTH
    const candidates = shuffle(SYMBOLS)
    while (result.length < size) {
      const symbol = candidates.pop()
      result.push(symbol, symbol)
    }
    return shuffle(result)
  }

  handleCardClick = (index) => {
    const { currentPair } = this.state

    if (currentPair.length === 2) {
      return
    }

    if (currentPair.length === 0) {
      this.setState({ currentPair: [index] })
      return
    }

    this.handleNewPairClosedBy(index)
  }

  getVisibilityForCard(index) {
    const { currentPair, matchedCardIndex } = this.state
    const indexMatched = matchedCardIndex.includes(index)
  
    if (currentPair.length < 2) {
      return indexMatched || index === currentPair[0] ? 'visible' : 'hidden'
    }
  
    if (currentPair.includes(index)) {
      return indexMatched ? 'justMatched' : 'justMismatched'
    }
  
    return indexMatched ? 'visible' : 'hidden'
  }

  handleNewPairClosedBy(index) {
    const { cards, currentPair, guesses, matchedCardIndex } = this.state

    const newPair = [currentPair[0], index]
    const newGuesses = guesses + 1
    const matched = cards[newPair[0]] === cards[newPair[1]]
    this.setState({ currentPair: newPair, guesses: newGuesses })
    if (matched) {
      this.setState({ matchedCardIndex: [...matchedCardIndex, ...newPair] })
    }
    setTimeout(() => this.setState({ currentPair: [] }), VISUAL_PAUSE_MSECS)
  }

  render() {
    const { cards, guesses, matchedCardIndex } = this.state
    const won = matchedCardIndex.length === cards.length
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">Memory game</Navbar.Brand>
        </Navbar>
        <Container>
          <Row>
            <Col lg="3" md="2" xs="1"></Col>
            <Col lg="6" md="8" xs="10">
              <Row xs="5">
                <Col xs="10"></Col>
                <Col xs="2"><GuessCount guesses={ guesses } /></Col>
                { cards.map((symbol, index) => (
                  <Col className="mt-3">
                    <Card
                      symbol={ symbol }
                      visibility={ this.getVisibilityForCard(index) }
                      index={ index }
                      key={ index }
                      onClick={ this.handleCardClick }
                    />
                  </Col>
                )) }
              </Row>
              { won && <HallOfFame entries={FAKE_HOF} /> }
            </Col>
            <Col lg="3" md="2" xs="1"></Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default App