import React from 'react'
import PropTypes from 'prop-types'
import Image from 'react-bootstrap/Image'

import './Card.css'

import verso from './verso.png'
import card0 from './card0.png'
import card1 from './card1.png'
import card2 from './card2.png'
import card3 from './card3.png'
import card4 from './card4.png'
import card5 from './card5.png'
import card6 from './card6.png'
import card7 from './card7.png'
import card8 from './card8.png'
import card9 from './card9.png'

const HIDDEN_SYMBOL = '?'

const Card = ({ symbol, visibility, index, onClick}) => (
    <div className={ `card ${visibility}` } onClick={ () => onClick(index) }>
      { visibility === 'hidden'
        ? <Image fluid thumbnail src={ verso } alt="verso" />
        : getCard(symbol) }
    </div>
)

Card.propTypes = {
  symbol: PropTypes.string.isRequired,
  visibility: PropTypes.oneOf([
    'hidden',
    'justMatched',
    'justMismatched',
    'visible',
  ]).isRequired,
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
}

function getCard(symbol) {
  switch(symbol) {
    case "0":
      return <Image fluid thumbnail src={ card0 } alt={ symbol } />;
    case "1":
      return <Image fluid thumbnail src={ card1 } alt={ symbol } />;
    case "2":
      return <Image fluid thumbnail src={ card2 } alt={ symbol } />;
    case "3":
      return <Image fluid thumbnail src={ card3 } alt={ symbol } />;
    case "4":
      return <Image fluid thumbnail src={ card4 } alt={ symbol } />;
    case "5":
      return <Image fluid thumbnail src={ card5 } alt={ symbol } />;
    case "6":
      return <Image fluid thumbnail src={ card6 } alt={ symbol } />;
    case "7":
      return <Image fluid thumbnail src={ card7 } alt={ symbol } />;
    case "8":
      return <Image fluid thumbnail src={ card8 } alt={ symbol } />;
    case "9":
      return <Image fluid thumbnail src={ card9 } alt={ symbol } />;
    default:
        return symbol;
  }
}

export default Card